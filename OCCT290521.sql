-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 03:00 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `OCCT290521`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/05/28 13:16:04', '2021-05-28', '2021-05-28', 1, 'Abbott'),
(2, 'Sunil', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021/05/28 20:31:12', '2021-05-28', '2021-05-28', 1, 'Abbott'),
(3, 'susanta pradhan', 'susantapradhan77@gmail.com', 'Bhubaneswar', 'Utkal hospital', NULL, NULL, '2021/05/28 20:56:21', '2021-05-28', '2021-05-28', 1, 'Abbott'),
(4, 'susanta pradhan', 'susantapradhan77@gmail.com', 'bhubaneswar', 'Utkal hospital', NULL, NULL, '2021/05/28 21:06:44', '2021-05-28', '2021-05-28', 1, 'Abbott'),
(5, 'DR.J.K.PADHI', 'cardiojkp@yahoo.com', 'BHUBANESWAR', 'AMRI HOSPITAL', NULL, NULL, '2021/05/29 18:23:54', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(6, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'Av', NULL, NULL, '2021/05/29 18:24:08', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(7, 'Dr.Biswaranjan Jena', 'drbrjena@gmail.com', 'cuttack', 'Ashwini ', NULL, NULL, '2021/05/29 18:24:37', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(8, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/29 18:29:45', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(9, 'dr sarat kumar sahjoo', 'drsaratsahoo@rediffmail.com', 'bhubanewsar', 'SUMUM', NULL, NULL, '2021/05/29 18:39:22', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(10, 'DR.M.P.Tripathy', 'drmptripathy@gmail.com', 'Bhubaneswar', 'CARE HOSPITALS', NULL, NULL, '2021/05/29 18:48:49', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(11, 'dr sarat kumar sahjoo', 'drsaratsahoo@rediffmail.com', 'bhubanewsar', 'SUMUM', NULL, NULL, '2021/05/29 18:49:11', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(12, 'Dipankar Das', 'dipankar.das@abbott.com', 'Kolkata', 'Abbott Vascular', NULL, NULL, '2021/05/29 18:50:08', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(13, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/29 18:51:10', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(14, 'Dibya Ranjan Behera', 'drdibya26@gmail.com', 'Bhubaneswar', 'SUM', NULL, NULL, '2021/05/29 18:52:34', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(15, 'Anupam Jena', 'dranupamjena@gmail.com', 'BHUBANESWAR', 'KALINGA INSTITUTE OF MEDICAL SCIENCES', NULL, NULL, '2021/05/29 18:53:28', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(16, 'susanta pradhan', 'susantapradhan77@gmail.com', 'bhubaneswar', 'utkal hospital', NULL, NULL, '2021/05/29 19:00:35', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(17, 'susanta pradhan', 'susantapradhan77@gmail.com', 'bhubaneswar', 'utkal hospital', NULL, NULL, '2021/05/29 20:08:17', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(18, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/31 19:44:03', '2021-05-31', '2021-05-31', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'susanta shaila', 'drsusantashaila@gmail.com', 'elective doube stent strategy is ideal in this case . nice decision', '2021-05-29 19:29:34', 'Abbott', 1, 0),
(2, 'Dr. Dipak Narayan Lenka', 'lenkadipaknarayan@gmail.com', 'What are the stents in market that can go 3 mm to 4.5 or 5 mm in cases of LMCA to LAD Stenting...', '2021-05-29 19:41:31', 'Abbott', 1, 0),
(3, 'susanta shaila', 'drsusantashaila@gmail.com', 'LADcaudal view appeared like trifurcation and after stenting ramus got compromised (appears). otherwise result is good. not quite sure as it was hazzy', '2021-05-29 19:53:13', 'Abbott', 1, 0),
(4, 'susanta shaila', 'drsusantashaila@gmail.com', 'why did you first adress LAD lesion ? as CTO should be adressed first as in case of CTO failure pt can be referred for CABG ', '2021-05-29 20:09:42', 'Abbott', 1, 1),
(5, 'susanta shaila', 'drsusantashaila@gmail.com', 'which technique (drilling or penetration) is suitable for this case ?', '2021-05-29 20:18:06', 'Abbott', 1, 0),
(6, 'susanta shaila', 'drsusantashaila@gmail.com', 'excellent result', '2021-05-29 20:18:47', 'Abbott', 1, 0),
(7, 'susanta shaila', 'drsusantashaila@gmail.com', 'excellent result Divya .congratulation', '2021-05-29 20:30:26', 'Abbott', 1, 0),
(8, 'Jyotirmaya sahoo', 'jyotirmaya1210@gmail.com', 'Howmuch safe to inject dye into conus branch repeatedly ??', '2021-05-29 20:34:55', 'Abbott', 1, 0),
(9, 'Panchanan Sahoo', 'sahoocardio@gmail.com', 'Dr Behera in first case did you plan LCX & RCA ', '2021-05-29 20:35:51', 'Abbott', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-28 16:00:29', '2021-05-28 16:00:29', '2021-05-28 17:30:29', 0, 'Abbott', '9121614c4482726dd9ebe686caff7090'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-28 16:27:59', '2021-05-28 16:27:59', '2021-05-28 16:28:13', 0, 'Abbott', '46a00667115ff2ea7ec0d3e709f471cb'),
(3, 'suvendu mandal', 'suvendumandal24@gmail.com', 'Bhubaneshwar', 'Utkal hospital, Bhubaneswar ', NULL, NULL, '2021-05-29 10:03:12', '2021-05-29 10:03:12', '2021-05-29 11:33:12', 0, 'Abbott', '4d5d288fb1f5d82b57fb328baaa86e56'),
(4, 'Bishnudev Pothal ', 'bishnudepothal@gmail.com', 'Kolkata ', 'Health Care ', NULL, NULL, '2021-05-29 12:12:00', '2021-05-29 12:12:00', '2021-05-29 13:42:00', 0, 'Abbott', 'e8d9b75492834e521bde6ac7ad8a4d0f'),
(5, 'Balaram', 'balaram_mahapatro@yahoo.co.in', 'Vizag', 'Medicover', NULL, NULL, '2021-05-29 17:29:39', '2021-05-29 17:29:39', '2021-05-29 18:59:39', 0, 'Abbott', '70294fe9ec1609b1320fde07b39b9ef7'),
(6, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-29 17:31:21', '2021-05-29 17:31:21', '2021-05-29 17:31:26', 0, 'Abbott', 'a78a44f86f9b9968bc9a569d4dbf7641'),
(7, 'Nishanth S', 'test@test.com', 'test', 'test', NULL, NULL, '2021-05-29 17:36:25', '2021-05-29 17:36:25', '2021-05-29 17:36:28', 0, 'Abbott', '699c835c3a0d83c4d04b8d25e32779b4'),
(8, 'Avijit Das', 'avijit.das1@abbott.com', 'KOLKATA', 'None', NULL, NULL, '2021-05-29 18:24:19', '2021-05-29 18:24:19', '2021-05-29 19:54:19', 0, 'Abbott', 'ddbf6cb6df1b7450dd805830d862e1cb'),
(9, 'Debasish Mohapatra', 'debasish304@gmail.com', 'Kolkata', 'RG KAR MCH', NULL, NULL, '2021-05-29 18:26:21', '2021-05-29 18:26:21', '2021-05-29 19:56:21', 0, 'Abbott', 'c998d31aa5ae59e8483e0cea741cee00'),
(10, 'Mrutyunjaya Behera', 'mbehera2002@yahoo.com', 'Cuttack', 'Aswini hospital', NULL, NULL, '2021-05-29 18:27:19', '2021-05-29 18:27:19', '2021-05-29 19:57:19', 0, 'Abbott', 'dc86d1d011a3f50e384d7d28dc47fd14'),
(11, 'Bishnudev Pothal', 'bishnudevpothal@gmail.com', 'Kolkata', 'Health Care ', NULL, NULL, '2021-05-29 18:41:50', '2021-05-29 18:41:50', '2021-05-29 18:51:15', 0, 'Abbott', 'c0e32db14dc0fd3b39decae16578bd9c'),
(12, 'BISHNUDEV POTHAL', 'bishnudevpothal@gmail.com', 'Kolkata', 'Health Care', NULL, NULL, '2021-05-29 18:52:05', '2021-05-29 18:52:05', '2021-05-29 19:00:56', 0, 'Abbott', 'bac88808f80c1ae289d2cffca64aa8a2'),
(13, 'PRAVAT RANJAN MOHAPATRA', 'drpravat78@gmail.com', 'Bhubaneswar', 'Vikash hospital', NULL, NULL, '2021-05-29 18:53:53', '2021-05-29 18:53:53', '2021-05-29 20:23:53', 0, 'Abbott', 'edc52e9f09645462dc6cd35b4755a436'),
(14, 'Dr.N.K.Mohanty', 'diptinirmal.pattnaik@gmail.com', 'Cuttack', 'SCB Medical College,Cuttack.', NULL, NULL, '2021-05-29 18:54:35', '2021-05-29 18:54:35', '2021-05-29 18:54:45', 0, 'Abbott', '8c967bbadac750ccae6cda4ad498c855'),
(15, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 18:55:01', '2021-05-29 18:55:01', '2021-05-29 20:25:01', 0, 'Abbott', '160ec1b2c855c6552f3b8de314e27a16'),
(16, 'Debasish Mohapatra', 'debasish304@gmail.com', 'Kolkata', 'RG Kar MCH', NULL, NULL, '2021-05-29 18:55:28', '2021-05-29 18:55:28', '2021-05-29 20:25:28', 0, 'Abbott', 'd24e1d9609a9ac853c58eca4ad871335'),
(17, 'MANAS RANJAN BARIK', 'manasbarik86@gmail.com', 'BHUBANESWAR', 'SUM ULTIMATE MEDICARE', NULL, NULL, '2021-05-29 18:57:51', '2021-05-29 18:57:51', '2021-05-29 20:27:51', 0, 'Abbott', 'd1e17bc4cc4a7ba8480bc35e19b3367b'),
(18, 'Ashwani', 'kumar.ashwin1326@gmail.com', 'Bhubaneswar', 'Care', NULL, NULL, '2021-05-29 18:59:55', '2021-05-29 18:59:55', '2021-05-29 19:00:59', 0, 'Abbott', 'c0158a41f365226ae56f927612e79b2e'),
(19, 'Dr.Nirmal Kumar Mohanty', 'diptinirmal.pattnaik@gmail.com', 'Cuttack', 'SCB Medical College,Cuttack.', NULL, NULL, '2021-05-29 18:59:58', '2021-05-29 18:59:58', '2021-05-29 19:00:12', 0, 'Abbott', '835187cba6a69c379fa093d5a552046a'),
(20, 'Manas Ranjan Barik', 'manasbarik86@gmail.com', 'Bhubaneswar', 'SUM ULTIMATE', NULL, NULL, '2021-05-29 19:00:37', '2021-05-29 19:00:37', '2021-05-29 20:30:37', 0, 'Abbott', '31b6dd4a049f3231f8ae3125b26e428b'),
(21, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 19:00:47', '2021-05-29 19:00:47', '2021-05-29 20:30:47', 0, 'Abbott', '32c8b294db4e82057deb15a139296539'),
(22, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-05-29 19:00:59', '2021-05-29 19:00:59', '2021-05-29 20:30:59', 0, 'Abbott', 'c71ed1fcf09e71b91039d049bd6368e5'),
(23, 'Ashwani', 'kumar.ashwin1326@gmail.com', 'Bhubaneswar', 'Care hospital', NULL, NULL, '2021-05-29 19:01:14', '2021-05-29 19:01:14', '2021-05-29 20:31:14', 0, 'Abbott', '0b67412d5f7bfe6af1ebbf04283b90b0'),
(24, 'PRAVAT RANJAN MOHAPATRA', 'drpravat78@gmail.com', 'Bargarh', 'Vikash hospital', NULL, NULL, '2021-05-29 19:01:17', '2021-05-29 19:01:17', '2021-05-29 20:31:17', 0, 'Abbott', '5ab30ab249e1a264f042a59c62d384ab'),
(25, 'DR. BISWAJIT DAS', 'drbisawjitdas67@gmail.com', 'CUTTACK', 'S.C.B.Medical College', NULL, NULL, '2021-05-29 19:01:34', '2021-05-29 19:01:34', '2021-05-29 20:31:34', 0, 'Abbott', 'f5940fbd680ca47ba423100d926901dc'),
(26, 'Ag', 'arindamg910@ail.com', 'Kol', 'KIMS', NULL, NULL, '2021-05-29 19:01:58', '2021-05-29 19:01:58', '2021-05-29 20:31:58', 0, 'Abbott', 'baef3cbcc28f83b597917cdfc26f0691'),
(27, 'susanta shaila', 'drsusantashaila@gmail.com', 'bhubaneswar', 'kalinga hospital', NULL, NULL, '2021-05-29 19:02:01', '2021-05-29 19:02:01', '2021-05-29 20:13:37', 0, 'Abbott', '0cf6083752cac11bc35562445d68333b'),
(28, 'PAWAN', 'pawan@coact.co.in', 'Pune ', 'Yes ', NULL, NULL, '2021-05-29 19:02:04', '2021-05-29 19:02:04', '2021-05-29 20:32:04', 0, 'Abbott', '06901030585712dce58da1feaab79e35'),
(29, 'Bishnudev Pothal', 'bishnudevpothal@gmail.com', 'Kolkata', 'Health Care', NULL, NULL, '2021-05-29 19:02:20', '2021-05-29 19:02:20', '2021-05-29 20:32:20', 0, 'Abbott', '22e913ae255621d7d73a7df6e7f42d2e'),
(30, 'Anupam saha', 'anupmaitra_2008@yahoo.com', 'Kolkata ', 'Woodlands ', NULL, NULL, '2021-05-29 19:02:37', '2021-05-29 19:02:37', '2021-05-29 20:32:37', 0, 'Abbott', 'a40f8a24427caf19c6b39dbec71511e0'),
(31, 'Anupam saha', 'anupmaitra_2008@yahoo.com', 'Kolkata ', 'Woodlands ', NULL, NULL, '2021-05-29 19:04:45', '2021-05-29 19:04:45', '2021-05-29 20:34:45', 0, 'Abbott', 'ea11d3824b9b54462d40930b44406ad6'),
(32, 'Satya Prakash Sahoo', 'satyaprakashcathlab@gmail.com', 'Bhubaneswar', 'Sum ultimate Medicare', NULL, NULL, '2021-05-29 19:04:48', '2021-05-29 19:04:48', '2021-05-29 20:34:48', 0, 'Abbott', '217b83783bf1bb18bbd334b5f64a9e37'),
(33, 'Pooja', 'pooja@coact.co.in', 'Mumbai', 'Test', NULL, NULL, '2021-05-29 19:05:10', '2021-05-29 19:05:10', '2021-05-29 20:35:10', 0, 'Abbott', '788c869b14a016b990ca458eb52373c5'),
(34, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-29 19:05:12', '2021-05-29 19:05:12', '2021-05-29 19:05:35', 0, 'Abbott', '7be4bcb61e63948933e83d5981b601f7'),
(35, 'Satyaranjan  Mohanty', 'satya.bapun@gmail.com', 'BHUBANESWAR', 'Apollo Hospitals ', NULL, NULL, '2021-05-29 19:06:13', '2021-05-29 19:06:13', '2021-05-29 20:36:13', 0, 'Abbott', 'c0b432ee0f80764be845a47c36254b1b'),
(36, 'Aditya Bhaskar', 'dradityabhaskarmd@gmail.com', 'Guwahati ', 'ASH', NULL, NULL, '2021-05-29 19:06:18', '2021-05-29 19:06:18', '2021-05-29 19:07:37', 0, 'Abbott', '8a61bacea66c0435a615853354d6af52'),
(37, 'Samir nayak', 'samirnayak703@gmail.com', 'Bhubaneswar', 'Apollo', NULL, NULL, '2021-05-29 19:06:24', '2021-05-29 19:06:24', '2021-05-29 20:36:24', 0, 'Abbott', '26593127b7ddce2aeb005e25c3d72f71'),
(38, 'Atanu Banerjee ', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-05-29 19:06:39', '2021-05-29 19:06:39', '2021-05-29 20:36:39', 0, 'Abbott', '22e167572e1d51808ceac49245a5f227'),
(39, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021-05-29 19:06:45', '2021-05-29 19:06:45', '2021-05-29 20:36:45', 0, 'Abbott', '271dd3bae2e03cbf4136ba77aae4c317'),
(40, 'Vibhutendra Mohanty ', 'drvm7578@gmail.com', 'Bhubaneswar ', 'Sum Ultimate Medicare ', NULL, NULL, '2021-05-29 19:08:11', '2021-05-29 19:08:11', '2021-05-29 20:38:11', 0, 'Abbott', 'a88de94f33f5277b899142e5ce3f72d0'),
(41, 'Manas Ranjan Barik', 'manasbarik86@gmail.com', 'Bhubaneswar', 'SUM ULTIMATE', NULL, NULL, '2021-05-29 19:08:41', '2021-05-29 19:08:41', '2021-05-29 20:38:41', 0, 'Abbott', 'e2e999d066d6725014c643f8cd7d3f6c'),
(42, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-29 19:08:47', '2021-05-29 19:08:47', '2021-05-29 19:09:11', 0, 'Abbott', '8b763cdf9569f0fbd3822ee3710a0e35'),
(43, 'suvendu mandal', 'suvendumandal24@gmail.com', 'Bhubaneshwar', 'Utkal hospital, Bhubaneswar ', NULL, NULL, '2021-05-29 19:08:53', '2021-05-29 19:08:53', '2021-05-29 20:38:53', 0, 'Abbott', 'e89a3560eba03f8f13861470ff2b46ea'),
(44, 'PRAVAT RANJAN MOHAPATRA', 'drpravat78@gmail.com', 'Bargarh', 'Vikash hospital', NULL, NULL, '2021-05-29 19:09:44', '2021-05-29 19:09:44', '2021-05-29 20:39:44', 0, 'Abbott', '65f7b14dd3b611a19025e38bd2e24361'),
(45, 'Saroj kumar choudhury', 'skc15788@gmail.com', 'Cuttack, Odisha', 'SCBMCH', NULL, NULL, '2021-05-29 19:10:27', '2021-05-29 19:10:27', '2021-05-29 20:40:27', 0, 'Abbott', '66e5aa23fe7ab5c14b88d3770d663644'),
(46, 'Rajiv', 'zooush@gmail.com', 'Delhi', 'RML', NULL, NULL, '2021-05-29 19:10:52', '2021-05-29 19:10:52', '2021-05-29 20:40:52', 0, 'Abbott', 'c6407f48c12353a79453fde20bf529d6'),
(47, 'Sitarasmi ', 'sitarasmi@gmail.com', 'Bhubaneswar ', 'Sum ultimate ', NULL, NULL, '2021-05-29 19:11:09', '2021-05-29 19:11:09', '2021-05-29 20:41:09', 0, 'Abbott', '90d72dbde6ad788aada4bae2e9b5fe49'),
(48, 'Priyabrata sahu', 'pspriyabrata@gmail.com', 'Bhubaneswar', 'Apollo', NULL, NULL, '2021-05-29 19:11:23', '2021-05-29 19:11:23', '2021-05-29 20:41:23', 0, 'Abbott', 'bdc971feadb5616cd5088d88956c65f4'),
(49, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-05-29 19:11:51', '2021-05-29 19:11:51', '2021-05-29 20:41:51', 0, 'Abbott', '21591b0b20680aa38dfb8d1a7edde9e7'),
(50, 'Girija Shankar Dr', 'girijashankarjha@gmail.com', 'Pokhara', 'Manipal ', NULL, NULL, '2021-05-29 19:13:14', '2021-05-29 19:13:14', '2021-05-29 20:43:14', 0, 'Abbott', '53823b8b6e10566ed5d50c1e3c761fcd'),
(51, 'Avijit', 'avijitbmb@gmail.com', 'Bhubaneswar', 'Care Hospital', NULL, NULL, '2021-05-29 19:13:15', '2021-05-29 19:13:15', '2021-05-29 20:43:15', 0, 'Abbott', '56a4b282e56d518701146a3143e52688'),
(52, 'Dr. Dipak Narayan Lenka', 'lenkadipaknarayan@gmail.com', 'Cuttack', 'Hi-tech Mch', NULL, NULL, '2021-05-29 19:13:56', '2021-05-29 19:13:56', '2021-05-29 20:43:56', 0, 'Abbott', '56bd677b8d94fb0ba1263c18487c47a5'),
(53, 'Dr Manabhanjan Jena', 'jena.mana@gmail.com', 'Bhubaneswar ', 'IMS, SUM Hospital', NULL, NULL, '2021-05-29 19:18:42', '2021-05-29 19:18:42', '2021-05-29 20:48:42', 0, 'Abbott', 'a6c614463dc8922153765bc8920aeba2'),
(54, 'Susanta shaila', 'drsusantashaila@gmail.com', 'Bhubaneswar ', 'Kalinga jospital', NULL, NULL, '2021-05-29 19:19:37', '2021-05-29 19:19:37', '2021-05-29 20:49:37', 0, 'Abbott', '7d4efda4a70c843935fc72dc12ceaa1a'),
(55, 'Ag', 'arindamg910@gmail.com', 'Kol', 'Kims', NULL, NULL, '2021-05-29 19:20:17', '2021-05-29 19:20:17', '2021-05-29 20:50:17', 0, 'Abbott', 'e24dc3218ae415e01609249d3e964088'),
(56, 'Surya', 'drskjena3095@gmail.com', 'Bhubaneswar ', 'Kims', NULL, NULL, '2021-05-29 19:21:06', '2021-05-29 19:21:06', '2021-05-29 20:51:06', 0, 'Abbott', '9798afb299d4caf4c75a68065baea17e'),
(57, 'Dibyajyoti Mishra', 'dibyajyoti.mishra@kims.ac.in', 'Bhubaneswar ', 'Kims ', NULL, NULL, '2021-05-29 19:23:59', '2021-05-29 19:23:59', '2021-05-29 20:53:59', 0, 'Abbott', 'bc210beaa6fbef806ca7544a2b2ab98f'),
(58, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 19:26:16', '2021-05-29 19:26:16', '2021-05-29 20:56:16', 0, 'Abbott', '4f63791188c7d7a3532f547e3f575b5e'),
(59, 'Dr Manabhanjan Jena', 'jena.mana@gmail.com', 'Bhubaneswar ', 'IMS, SUM Hospital', NULL, NULL, '2021-05-29 19:28:57', '2021-05-29 19:28:57', '2021-05-29 20:58:57', 0, 'Abbott', '68e6c4c5915e49cb6c317520e3ce61ad'),
(60, 'PRIYARANJAN PARIDA', 'priyaranjan74@gmail.com', 'Cuttack', 'SCB', NULL, NULL, '2021-05-29 19:29:51', '2021-05-29 19:29:51', '2021-05-29 20:59:51', 0, 'Abbott', '9fd8e1147cae322c745a3c397d323e7f'),
(61, 'DEEPAK KUMAR PARHI', 'dr.deepakparhi@gmail.com', 'Bhubaneswar ', 'Ims and sum', NULL, NULL, '2021-05-29 19:31:23', '2021-05-29 19:31:23', '2021-05-29 21:01:23', 0, 'Abbott', 'f7770fe3d644586b07abfc509e625b8d'),
(62, 'Panchanan Sahoo', 'sahoocardio@gmail.com', 'bhubaneswar', 'kims', NULL, NULL, '2021-05-29 19:32:07', '2021-05-29 19:32:07', '2021-05-29 21:02:07', 0, 'Abbott', '8fa1357c4d7e682959517983c4a64067'),
(63, 'Dr chandrakanta Mishra', 'drckmishra@gmail.com', 'Cuttack ', 'S C B Medical college ', NULL, NULL, '2021-05-29 19:32:49', '2021-05-29 19:32:49', '2021-05-29 21:02:49', 0, 'Abbott', '33d5bca14b8885b94e9e2a0e28f7c12f'),
(64, 'Jyotirmaya sahoo', 'jyotirmaya1210@gmail.com', 'Delhi', 'Manipal hospital', NULL, NULL, '2021-05-29 19:33:06', '2021-05-29 19:33:06', '2021-05-29 21:03:06', 0, 'Abbott', '643e98c65066f3043e24468d9bc374f3'),
(65, 'Satyaranjan  Mohanty', 'satya.bapun@gmail.com', 'BHUBANESWAR', 'Apollo Hospitals ', NULL, NULL, '2021-05-29 19:33:37', '2021-05-29 19:33:37', '2021-05-29 21:03:37', 0, 'Abbott', 'dfc020c114211aebae0b8faadf277c0f'),
(66, 'PRAVAT RANJAN MOHAPATRA', 'drpravat78@gmail.com', 'Bargarh', 'Vikash hospital', NULL, NULL, '2021-05-29 19:35:51', '2021-05-29 19:35:51', '2021-05-29 21:05:51', 0, 'Abbott', '424553e5841ba05e3db0f3f0ece9e7ed'),
(67, 'Argha Dasmahapatra', 'dasmahapatraargha@gmail.com', 'Bhubaneswar', 'IMS & SUM Hospital', NULL, NULL, '2021-05-29 19:36:17', '2021-05-29 19:36:17', '2021-05-29 21:06:17', 0, 'Abbott', '1d4b16f8feaea32f2531d4f490d981aa'),
(68, 'Argha Dasmahapatra', 'dasmahapatraargha@gmail.com', 'Bhubaneswar', 'IMS & SUM Hospital', NULL, NULL, '2021-05-29 19:36:34', '2021-05-29 19:36:34', '2021-05-29 21:06:34', 0, 'Abbott', 'd1d8ce65ac1a6dc7323b5c905d5b824b'),
(69, 'Dr SSL Bajoria', 'drssbajoria2@gmail.com', 'Cuttack', 'nil', NULL, NULL, '2021-05-29 19:43:07', '2021-05-29 19:43:07', '2021-05-29 21:13:07', 0, 'Abbott', '6ba5aaa55f2bc5130083152282d735a7'),
(70, 'Saurav Das', 'drsaurav1@gmail.com', 'Bhubhaneswar', 'KIMS ', NULL, NULL, '2021-05-29 19:43:34', '2021-05-29 19:43:34', '2021-05-29 21:13:34', 0, 'Abbott', 'bd6b61cda68e018bd18d550dc75ea3fa'),
(71, 'Dibyajyoti Mishra', 'dibyajyoti.mishra@kims.ac.in', 'Bhubaneswar ', 'Kims Bhubaneswar ', NULL, NULL, '2021-05-29 19:46:40', '2021-05-29 19:46:40', '2021-05-29 21:16:40', 0, 'Abbott', 'e1a0b64a743e987bbbe03642cdbc23c4'),
(72, 'Sunil Mishra', 'dr.sunil46@gmail.com', 'bhubaneswar', 'Sumum', NULL, NULL, '2021-05-29 19:48:41', '2021-05-29 19:48:41', '2021-05-29 21:18:41', 0, 'Abbott', '9b535389e676cf2a2065765f2adc4f45'),
(73, 'Milan ghadei', 'drmilan80@gmail.com', ' Bhubaneswar', 'Sum ultimate medicare', NULL, NULL, '2021-05-29 19:53:00', '2021-05-29 19:53:00', '2021-05-29 21:23:00', 0, 'Abbott', '6c3b6a1fdf7590674568ca56ed5f840b'),
(74, 'Jagannath Dash', 'dashjagannath2014@gmail.com', 'Bhubaneswar', 'AIIMS', NULL, NULL, '2021-05-29 20:10:07', '2021-05-29 20:10:07', '2021-05-29 21:40:07', 0, 'Abbott', '84a68ad5d2a3338c0e1add4a0151c19c'),
(75, 'Dr Ranjan Kumar Mohanty', 'ranjan_silu@rediffmail.com', 'Cuttack', 'Scb', NULL, NULL, '2021-05-29 20:10:13', '2021-05-29 20:10:13', '2021-05-29 21:40:13', 0, 'Abbott', 'eebdff0e831a875a76e35c5c5ca3b2fb'),
(76, 'Dr Ranjan Kumar Mohanty', 'ranjan_silu@rediffmail.com', 'Cuttack', 'Scb', NULL, NULL, '2021-05-29 20:11:55', '2021-05-29 20:11:55', '2021-05-29 21:41:55', 0, 'Abbott', '47bc75ace9d3addc25a88a08730cf432'),
(77, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-05-29 20:12:27', '2021-05-29 20:12:27', '2021-05-29 21:42:27', 0, 'Abbott', '28806f101a2f1ae4338afc1fdcbc3de3'),
(78, 'PAWAN', 'pawan@coact.co.in', 'Yes ', 'Yes ', NULL, NULL, '2021-05-29 20:12:39', '2021-05-29 20:12:39', '2021-05-29 21:42:39', 0, 'Abbott', 'c0e1830c8c97abf82f2e9a9d64252068'),
(79, 'Dr Girija Shankar Jha ', 'girijashankarjha@gmail.com', 'Kolkata ', 'Manipal hospital ', NULL, NULL, '2021-05-29 20:13:45', '2021-05-29 20:13:45', '2021-05-29 21:43:45', 0, 'Abbott', 'e5dfbf2ec90fca88a631d07b286135cb'),
(80, 'Priyabrata sahu', 'pspriyabrata@gmail.com', 'Bhubaneswar', 'Apollo', NULL, NULL, '2021-05-29 20:13:46', '2021-05-29 20:13:46', '2021-05-29 21:43:46', 0, 'Abbott', 'b85ee485688e7249887a6d02704c924f'),
(81, 'susanta shaila', 'drsusantashaila@gmail.com', 'bhubaneswar', 'kalinga hospital', NULL, NULL, '2021-05-29 20:16:27', '2021-05-29 20:16:27', '2021-05-29 20:36:34', 0, 'Abbott', 'dd802fa322e14016399c6ca3cf499da4'),
(82, 'Sabyasachi Raut', 'Paragarya4@gmail.com', 'Puri', 'Apollo', NULL, NULL, '2021-05-29 20:17:53', '2021-05-29 20:17:53', '2021-05-29 21:47:53', 0, 'Abbott', '0b9c0b4c1cbabe440efa9ec44ead2a3d'),
(83, 'Manas Ranjan Barik', 'manasbarik86@gmail.com', 'Bhubaneswar', 'SUM ULTIMATE', NULL, NULL, '2021-05-29 20:19:45', '2021-05-29 20:19:45', '2021-05-29 21:49:45', 0, 'Abbott', '185d5f2e6c6339f131cfff176157bef2'),
(84, 'DR. BISWAJIT DAS', 'drbisawjitdas67@gmail.com', 'CUTTACK', 'S.C.B.Medical College', NULL, NULL, '2021-05-29 20:23:29', '2021-05-29 20:23:29', '2021-05-29 21:53:29', 0, 'Abbott', 'a95f6b08bc5a1859b2a110385dd4cad6'),
(85, 'Mrutyunjaya Behera', 'mbehera2002@yahoo.com', 'Cuttack', 'Aswini', NULL, NULL, '2021-05-29 20:29:51', '2021-05-29 20:29:51', '2021-05-29 21:59:51', 0, 'Abbott', '210db0d99ec9c672bfe446a3010b7bcc'),
(86, 'Dr Manabhanjan Jena', 'jena.mana@gmail.com', 'Bhubaneswar ', 'IMS, SUM Hospital', NULL, NULL, '2021-05-29 20:31:19', '2021-05-29 20:31:19', '2021-05-29 22:01:19', 0, 'Abbott', '1ec043fbc823ac0cdee16d6f100c3d32'),
(87, 'Dr SSL Bajoria', 'drssbajoria2@gmail.com', 'Cuttack', 'nil', NULL, NULL, '2021-05-29 20:31:46', '2021-05-29 20:31:46', '2021-05-29 22:01:46', 0, 'Abbott', '099e15f23d5f6ef44a31c6c25f2ced2a'),
(88, 'PRAVAT RANJAN MOHAPATRA', 'drpravat78@gmail.com', 'Bargarh', 'Vikash hospital', NULL, NULL, '2021-05-29 20:39:35', '2021-05-29 20:39:35', '2021-05-29 22:09:35', 0, 'Abbott', 'a8ebd328df03907cfc71bbccffa7a7c3'),
(89, 'DR. BISWAJIT DAS', 'drbisawjitdas67@gmail.com', 'CUTTACK', 'S.C.B.Medical College', NULL, NULL, '2021-05-29 20:43:08', '2021-05-29 20:43:08', '2021-05-29 20:51:31', 0, 'Abbott', 'ef9876546b13d1229a9cb6ee412d301b'),
(90, 'DR RAJESH JHA', 'raj_ranchi99@yahoo.com', 'RANCHI', 'Artemis hospitals, Raj hospitals', NULL, NULL, '2021-05-29 20:52:10', '2021-05-29 20:52:10', '2021-05-29 21:01:53', 0, 'Abbott', 'f6b2def07dba42320f0ea9ebb4b83c4b'),
(91, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 20:56:33', '2021-05-29 20:56:33', '2021-05-29 22:26:33', 0, 'Abbott', '23389465c54cf3744bd7edbe62f9ef00'),
(92, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 21:04:29', '2021-05-29 21:04:29', '2021-05-29 21:04:38', 0, 'Abbott', '2c9fab391cca96e30da8194ca018fa1c'),
(93, 'Dr Girija Shankar Jha ', 'girijashankarjha@gmail.com', 'Kolkata ', 'Manipal hospital ', NULL, NULL, '2021-05-29 21:05:38', '2021-05-29 21:05:38', '2021-05-29 22:35:38', 0, 'Abbott', 'fdfd0c4ba1fcf1d58563981f7cf87c71'),
(94, 'Samir nayak', 'samirnayak703@gmail.com', 'Bhubaneswar', 'Apollo', NULL, NULL, '2021-05-29 22:16:16', '2021-05-29 22:16:16', '2021-05-29 23:46:16', 0, 'Abbott', '074d410372df270e85cedfe42fcc9a38'),
(95, 'Purusottam Mohapatra', 'purusottamm9@gmail.com', 'Bhubaneswar', 'Aiims bbsr', NULL, NULL, '2021-05-29 23:49:42', '2021-05-29 23:49:42', '2021-05-30 01:19:42', 0, 'Abbott', 'd39b5a26a784ec7bb2a7d92519e768c6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
